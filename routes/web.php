<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();


Route::namespace('Person')->prefix('person/')->group(function () {
    Route::get('add', 'PersonController@add')->name('person.add')->middleware('auth');
    Route::get('{id}', 'PersonController@view')->name('person.view');
    Route::get('{id}/edit', 'PersonController@edit')->name('person.edit')->middleware('auth');
    Route::post('{id}/update', 'PersonController@update')->middleware('auth');
    Route::post('store', 'PersonController@store')->middleware('auth');
});

Route::namespace('Address')->middleware(['check_person_exists'])->prefix('person/{person_id}/addresses/')->group(function () {
    Route::get('add', 'AddressController@add')->name('address.add');
    Route::get('{address_id}/edit', 'AddressController@edit')->middleware('auth');
    Route::get('{address_id}/delete', 'AddressController@delete')->middleware('auth');
    Route::post('{address_id}/update', 'AddressController@update')->middleware('auth');
    Route::post('store', 'AddressController@store')->middleware('auth');
});
