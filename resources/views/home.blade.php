@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Tüm Kişiler<span class="float-right"><a
                                href="/person/add">Yeni Kişi Ekle</a> </span></div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">İsim</th>
                                <th scope="col">Doğum Günü</th>
                                <th scope="col">Cinsiyet</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($people as $person)
                                <tr>
                                    <th scope="row">{{$person->id}}</th>
                                    <td><a href="/person/{{$person->id}}">{{$person->name}}</a></td>
                                    <td>{{$person->birthday}}</td>
                                    <td>{{$person->getGender()}}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td>Hiç kişi yok.</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
