@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@if($type == 'add'){{$person->name}} Kişisine Yeni Adres Ekle @elseif($type == 'edit') {{$person->name}} kişisinin üst bilgileri @endif</div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($type == 'add')
                            <form action="store" method="POST">
                                @elseif($type == 'edit')
                                    <form action="update" method="POST">
                                        @endif
                                        {!! csrf_field() !!}

                                        <div class="form-group">
                                            <label for="iAddress">Address</label>
                                            <textarea class="form-control" name ="address" required id="iAddress"  rows="3">@if($type == 'edit'){{$address->address}}@endif</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="iCityName">Şehir</label>
                                            <input id="iCityName" type="text" name="city_name" @if($type == 'edit') value="{{$address->city_name}}" @endif required class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="iCountryName">Ülke</label>
                                            <input id="iCountryName" type="text" name="country_name" @if($type == 'edit') value="{{$address->country_name}}" @endif required class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="iPostalCode">Posta Kodu</label>
                                            <input id="iPostalCode" type="text" name="postal_code" @if($type == 'edit') value="{{$address->postal_code}}" @endif required class="form-control">
                                        </div>

                                        <button type="submit" class="btn btn-primary mb-2">Kaydet</button>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
