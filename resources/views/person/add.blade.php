@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@if($type == 'add')Yeni Kişi Ekle @elseif($type == 'edit') {{$person->name}} kişisinin üst bilgileri @endif</div>

                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if($type == 'add')
                            <form action="store" method="POST">
                        @elseif($type == 'edit')
                            <form action="update" method="POST">
                        @endif
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="iName">İsim</label>
                                <input id="iName" type="text" name="name" @if($type == 'edit') value="{{$person->name}}" @endif required class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="iDate">Doğum Günü</label>
                                <input id="iDate" type="date" name="birthday" max="3000-12-31"
                                       min="1000-01-01" required class="form-control"
                                    @if($type == 'edit') value="{{$person->birthday}}" @endif
                                >
                            </div>

                            <div class="form-group">
                                <label for="iGender">Cinsiyet</label>
                                <select id="iGender" class="form-control" name="gender">
                                    @foreach($genders as $key =>    $value)
                                        <option value="{{$key}}" @if($type == 'edit') @if($person->gender == $key) selected @endif @endif >{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group d-none">
                                <label for="iImportant">Önemli Soru</label>
                                <input id="iImportant" type="text" name="important" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Kaydet</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
