@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h1>{{$person->name}} kişisi hakkında bilgiler</h1>
                        @auth
                            <a href="{{$person->id}}/edit">Kişi Üst Bilgisini Güncelle</a>
                        @endauth
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h2>Detaylar</h2>
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Doğum Günü
                                <span class="badge badge-primary badge-pill">{{$person->birthday}}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Cinsiyet
                                <span class="badge badge-primary badge-pill">{{$person->getGender()}}</span>
                            </li>
                        </ul>
                        <hr>
                        <h2>Adresler</h2>
                        @auth
                           <span class="float-right"> <a href="{{$person->id}}/addresses/add">Yeni Adres Ekle</a></span>
                        @endauth
                        <p>{{$person->name}} kişisine ait adres bilgileri aşağıda listelenmektedir.</p>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Adres</th>
                                    <th scope="col">Şehir</th>
                                    <th scope="col">Ülke</th>
                                    <th scope="col">Posta Kodu</th>
                                    @auth
                                    <th scope="col">Aksiyonlar</th>
                                    @endauth
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($person->addresses as $address)
                                    <tr>
                                        <th scope="row">{{$address->id}}</th>
                                        <td>{{$address->address}}</td>
                                        <td>{{$address->city_name}}</td>
                                        <td>{{$address->country_name}}</td>
                                        <td>{{$address->postal_code}}</td>
                                        @auth
                                        <td><a href="{{$person->id}}/addresses/{{$address->id}}/edit">Düzenle</a> - <a href="{{$person->id}}/addresses/{{$address->id}}/delete">Sil</a></td>
                                        @endauth
                                    </tr>
                                @empty
                                    <tr>
                                        <td>Hiç adres yok.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Recipient:</label>
                            <input type="text" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Message:</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send message</button>
                </div>
            </div>
        </div>
    </div>
@endsection
