<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    //
    const GENDERS = array(
        'none' => 'Belirtilmedi',
        'male' => 'Erkek',
        'female' => 'Kadın'
    );

    public function getGender()
    {
        return self::GENDERS[$this->gender];
    }

    public function addresses()
    {
        return $this->hasMany('App\Address', 'people_id', 'id');
    }
}
