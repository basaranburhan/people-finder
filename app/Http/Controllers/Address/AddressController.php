<?php

namespace App\Http\Controllers\Address;

use App\Address;
use App\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class AddressController extends Controller
{
    //
    public function add($person_id)
    {
        $person = Cache::rememberForever('person_' . $person_id, function () use ($person_id) {
            return Person::find($person_id);
        });
        return view('address.add', [
            'person' => $person,
            'type' => 'add'
        ]);
    }

    public function store($person_id, Request $request)
    {
        $validated_data = $request->validate([
            'city_name' => 'required',
            'country_name' => 'required',
            'postal_code' => 'required',
            'address' => 'required'
        ]);

        $address = new Address();
        $address->people_id = $person_id;
        $address->city_name = $request->city_name;
        $address->country_name = $request->country_name;
        $address->postal_code = $request->postal_code;
        $address->address = $request->address;
        $address->save();

        if (Cache::has('person_' . $person_id . '_addresses')) {
            Cache::forget('person_' . $person_id . '_addresses');
        }

        return redirect()->route('person.view', [$person_id]);
    }

    public function edit($person_id, $address_id)
    {
        $person = Cache::rememberForever('person_' . $person_id, function () use ($person_id) {
            return Person::find($person_id);
        });
        $address = Cache::rememberForever('person_' . $person_id . '_address_' . $address_id, function () use ($address_id) {
            return Address::find($address_id);
        });

        if ($address != null) {

            return view('address.add',
                [
                    'type' => 'edit',
                    'address' => $address,
                    'person' => $person
                ]);
        }
        return redirect()->route('home');
    }

    public function delete($person_id, $address_id)
    {

        $address = Cache::rememberForever('person_' . $person_id . '_address_' . $address_id, function () use ($address_id) {
            return Address::find($address_id);
        });
        $address->delete();
        Cache::forget('person_' . $person_id . '_address_' . $address_id);
        return redirect()->route('person.view', [$person_id]);

    }

    public function update($person_id, $address_id, Request $request)
    {
        $validated_data = $request->validate([
            'city_name' => 'required',
            'country_name' => 'required',
            'postal_code' => 'required',
            'address' => 'required'
        ]);
        $address = Cache::rememberForever('person_' . $person_id . '_address_' . $address_id, function () use ($address_id) {
            return Address::find($address_id);
        });
        if ($address != null) {
            $address->city_name = $request->city_name;
            $address->country_name = $request->country_name;
            $address->postal_code = $request->postal_code;
            $address->address = $request->address;
            $address->save();
            if (Cache::has('person_' . $person_id . '_address_' . $address_id)) {
                Cache::forget('person_' . $person_id . '_address_' . $address_id);
            }
            return redirect()->route('person.view', [$person_id]);
        }
        return redirect()->route('home');

    }
}
