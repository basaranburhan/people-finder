<?php

namespace App\Http\Controllers\Person;

use App\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class PersonController extends Controller
{
    //
    public function add()
    {

        return view('person.add', [
            'genders' => Person::GENDERS,
            'type' => 'add'
        ]);
    }

    public function view($person_id)
    {
        $person = Cache::rememberForever('person_' . $person_id, function () use ($person_id) {
            return Person::find($person_id);
        });
        if ($person != null) {

            return view('person.view', [
                'person' => $person
            ]);
        }
        return redirect()->route('home');

    }

    public function edit($person_id)
    {
        $person = Cache::rememberForever('person_' . $person_id, function () use ($person_id) {
            return Person::find($person_id);
        });
        if ($person != null) {

            return view('person.add', [
                'genders' => Person::GENDERS,
                'person' => $person,
                'type' => 'edit'
            ]);
        }
        return redirect()->route('home');
    }

    public function store(Request $request)
    {
        $validated_data = $request->validate([
            'name' => 'required',
            'birthday' => 'required|date',
            'gender' => 'required|in:male,female,none'
        ]);

        $person = new Person();
        $person->name = $request->name;
        $person->birthday = $request->birthday;
        $person->gender = $request->gender;
        $person->save();

        $people = Cache::get('people');
        $people[] = $person;
        Cache::put('people', $people);

        return redirect()->route('person.view', ['person_id' => $person->id]);

    }

    public function update($person_id, Request $request)
    {
        $validated_data = $request->validate([
            'name' => 'required',
            'birthday' => 'required|date',
            'gender' => 'required|in:male,female,none'
        ]);

        $person = Person::find($person_id);
        if ($person != null) {

            $person->name = $request->name;
            $person->birthday = $request->birthday;
            $person->gender = $request->gender;
            $person->save();
            if (Cache::has('people')) {
                Cache::forget('people');
            }
            if (Cache::has('person_' . $person_id)) {
                Cache::forget('person_' . $person_id);
            }
            return redirect()->route('person.view', ['person_id' => $person_id]);
        }
        return redirect()->route('home');

    }
}
