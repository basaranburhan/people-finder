<?php

namespace App\Http\Middleware;

use App\Person;
use Closure;
use Illuminate\Support\Facades\Cache;

class CheckPersonExists
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $array = explode("/", $request->getRequestUri());
        if (isset($array[2]) && is_numeric($array[2])) {
            $person_id = $array[2];

            $person = Cache::rememberForever('person_' . $person_id, function () use ($person_id) {
                return Person::find($person_id);
            });
            if ($person != null) {
                return $next($request);

            }
        }
        return redirect()->route('home');
    }
}
